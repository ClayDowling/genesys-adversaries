#include "expect.h"
#include "parseexception.h"

#include <sstream>
using std::ostringstream;

#include <cctype>
using std::isspace;

token expect_token(lexer& src, token_type T) {
    return expect_token(src, {T});
}

token expect_token(lexer& src, initializer_list<token_type> types) {
    auto tok = src.get();
    bool found = false;
    for(auto t : types) {
        if (tok.type == t)
            found = true;
    }
    if (found == false) {
        ostringstream msg;
        msg << "Expected ";
        for(auto p = types.begin(); p != types.end(); p++) {
            if (p != types.begin()) {
                msg << "/";
            }
            msg << *p;
        }
        msg << " but found " << tok.type;
        throw parse_exception(msg.str().c_str(), tok.line);
    }
    return tok;
}

string expect_name(lexer &src) {

    auto w = expect_token(src, {QUOTEDSTRING, WORD});
    if (w.type == QUOTEDSTRING) {

        bool isempty = true;
        for(auto c : w.value) {
            if (!isspace(c)) isempty = false;
        }

        if (isempty) {
            throw parse_exception("Cannot use empty strings for names", w.line);
        }

        return w.value;
    }

    string n = w.value;
    w = src.get();

    while (w.type == WORD) {
        n = n + ' ' + w.value;
        w = src.get();
    }
    src.put_back(w);

    return n;
}

int expect_number(lexer &src) {
    auto tok = expect_token(src, NUMBER);
    return std::stoi(tok.value);
}
