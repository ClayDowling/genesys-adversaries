#include "parseexception.h"

#include <cstring>
using ::strdup;

#include <sstream>
using std::ostringstream;

parse_exception::parse_exception(const char *msg, int n) : lineno(n) {
    if (n) {
        ostringstream realmsg;
        realmsg << msg << " at line " << n;
        message = strdup(realmsg.str().c_str());
    } else {
        message = strdup(msg);
    }
}

parse_exception::parse_exception(const char *msg) : lineno(0) {
    message = strdup(msg);
}

const char *parse_exception::what() const noexcept {
    return message;
}

ostream& operator<<(ostream& out, parse_exception& e) {
    if (!out) return out;
    out << "Parse Exception: " << e.what();
    return out;
}

