# Genesys Adversaries Builder

The [Genesys
RPG](https://www.fantasyflightgames.com/en/products/genesys/) is a great
game for certain over the top narrative role playing experiences.
There's even a handy shorthand system for creating NPCs found in the
[Expanded Player's
Guide](https://www.fantasyflightgames.com/en/products/genesys/).  The
Genesys Adversaries Builder is designed to support that system.

## Defining An Adversary

    minion Town Guard: Skilled Warrior, Dodgy, Soldier, Basic Melee Warrior

This shorthand, based on the adversary building system, would expand to
a regular stat block including skill rolls and weapon stats.


## Grammar Definition

### Terminals

```
WORD:           [A-Za-z][A-Za-z0-9"-"]*
NUMBER:         [-+]?[0-9]+
LPAREN:         '('
RPAREN:         ')'
COLON:          ':'
SEMICOLON:      ';'
COMMA:          ','
NEWLINE:        '\n'
ATTRIBUTE:      "Brawn"|"Agility"|"Intelligence"|"Cunning"|"Willpower"|"Presence"
RANGEBAND:      "engaged"|"close"|"medium"|"long"|"extreme"
SKILL:          "skill"
PACKAGE:        "package"
TALENT:         "talent"
WEAPON:         "weapon"
USE:            "use"
DAMAGE:         "damage"
CRITICAL:       "critical"
RANGE:          "range"
CHARACTER:      "minion"|"rival"|"nemesis"
QUOTEDSTRING:   '"'.*'"'
```

### Grammar

```
definitionList ::= definition definitionList

definition ::= def_skill NEWLINE | packageList NEWLINE 
             | def_talent NEWLINE | characterList NEWLINE

def_skill ::= skill COMMA def_skill | skill 

skill ::= SKILL name LPAREN ATTRIBUTE RPAREN

packageList ::= package packageList | package 

package ::= PACKAGE name COLON itemList NEWLINE

character ::= CHARACTER name COLON itemList NEWLINE

leveledItem ::= name NUMBER | name

itemList ::= item COMMA itemList | item

item ::= attributeBonus | leveledItem

attributeBonus ::= ATTRIBUTE NUMBER

name ::= WORD name | name | WORD

weapon ::= WEAPON name LPAREN name SEMICOLON DAMAGE number SEMICOLON CRITICAL number SEMICOLON RANGE RANGEBAND [SEMICOLON NAME ... ] RPAREN

```

## Semantic Processing

* All skill items are placed in a global skill dictionary.
* All talent items are placed in a global tallent dictionary.
* All package items are placed in a global package dictionary.
* leveledItems become skillReferences if the name appears in the skill
  dictionary.  They become talentReferences if the name appears in the talent
  dictionary.  If the name does not appear in either dictionary it is considered
  an error.
* characters are reduced to actual skills, talents, and attributes, then placed
  in the character dictionary.

## Output

All characters will be printed, unless the user has asked for only specific
characters.
