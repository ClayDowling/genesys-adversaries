#include <iostream>

using std::endl;

#include <sstream>

using std::ostringstream;

#include <memory>

using std::make_shared;

#include "model.h"
#include <ios>
using std::left;
using std::showpos;
using std::noshowpos;

#include <iomanip>

using std::setw;

#include <algorithm>
#include <cstring>

using std::find_if;


ostream &operator<<(ostream &out, const attribute_bonus &value) {
    if (out) {
        out << value.attribute << " ";
        out << showpos << value.bonus << noshowpos;
    }
    return out;
}

skill::skill(string n, attribute_t a) : name(n), attribute(a) {};

skill::skill(skill_t *s) {

    ostringstream text;
    text << s->name;

    name = s->name;
    attribute = s->attribute;
}

ostream &operator<<(ostream &out, const skill &s) {
    if (out) {
        out << s.name << " (" << s.attribute << ")";
    }
    return out;
}

skill_reference& skill_reference::operator+=(const leveledItem_t& rhs) {
    proficiency += rhs.level;
    return *this;
}
skill_reference& skill_reference::operator+=(const skill_reference& rhs) {
    proficiency += rhs.proficiency;
    return *this;
}

ostream &operator<<(ostream &out, const skill_reference &sr) {
    if (out) {
        out << *sr.parent << ": " << sr.proficiency;
    }
    return out;
}

talent::talent(talent_t *t) {
    if (t) {
        name = t->name;
    }
}

ostream &operator<<(ostream &out, const talent &t) {
    if (out) {
        out << t.name;
    }
    return out;
}

talent_reference& talent_reference::operator+=(const talent_reference& rhs) {
    level += rhs.level;
    return *this;
}

talent_reference& talent_reference::operator+=
        (const leveledItem_t& rhs) {
    level += rhs.level;
    return *this;
}

ostream &operator<<(ostream &out, const talent_reference &tr) {
    if (out) {
        out << *tr.parent;
        if (tr.level != 0) {
            out << ' ' << tr.level;
        }
    }
    return out;
}

ostream &operator<<(ostream &out, const weapon &w) {
    if (out) {
        out << w.name << " (" << w.skill << "; Damage ";
        if (w.brawl) out << "+";
        out << w.damage << "; Crit " << w.crit;
        if (w.special.size() > 0) {
            out << "; ";
            bool first = true;
            for (auto it: w.special) {
                if (first) {
                    first = false;
                } else {
                    out << ", ";
                }
                out << it;
            }
        }
        out << ")";
    }
    return out;
}

package::package(package_t *p) {
    name = p->name;
}

ostream &operator<<(ostream &out, const package &p) {
    if (out) {
        out << p.name << ": ";
        const char *sep = "";
        for (auto a: p.attributes) {
            out << sep << *a;
            sep = ", ";
        }
        for (auto t: p.talents) {
            out << sep << t.second;
            sep = ", ";
        }
        for (auto s: p.skills) {
            out << sep << s.second;
            sep = ", ";
        }
    }
    return out;
}

character::character() {
    for (int i = 0; i < attr_MAX; ++i)
        attribute[i] = 0;
}

character::character(const character_t *c) {
    name = c->name;
    for (int i = 0; i < attr_MAX; ++i)
        attribute[i] = 0;
}

int character::proficiency(const string skillname) {
    if (skills.contains(skillname)) {
        auto s = skills[skillname];
        return s.proficiency;
    }
    return 0;
}

int character::ability(const string skillname) {
    auto s = skills[skillname];
    auto prof = proficiency(skillname);

    return attribute[s.parent->attribute] - prof;
}

void character::shortform(ostream& out) {
    if (!out) return;

    out << name << endl;
    out << endl;
    out << "Br Ag Int Cun Will Pr" << endl
        << "-- -- --- --- ---- --" << endl;
    out << setw(2) << attribute[attr_brawn] << " "
        << setw(2) << attribute[attr_agility] << " "
        << setw(3) << attribute[attr_intellect] << " "
        << setw(3) << attribute[attr_cunning] << " "
        << setw(4) << attribute[attr_willpower] << " "
        << setw(2) << attribute[attr_presence] << endl << endl;

    if (!talents.empty()) {
        bool first = true;
        for (auto p: talents) {
            if (first) {
                first = false;
            } else {
                out << ", ";
            }
            out << p.second;
        }
        out << endl;
    }

    if (!skills.empty()) {
        bool first = true;
        for (auto p: skills) {
            if (first) {
                first = false;
            } else {
                out << ", ";
            }
            out << p.second;
        }
    }

    if (!weapons.empty()) {
        out << endl;
        for(auto p : weapons) {
            weapon w(*p.second);
            out << w << endl;
        }

    }
}

model::model() {}

model::model(definitionList_t &dl) {
    for (auto def: dl) {
        switch (def->type) {
            case def_skill:
                load_skilllist(def->skills);
                break;
            case def_talent:
                load_talenlist(def->talents);
                break;
            case def_package:
                load_package(def->package);
                break;
            case def_character:
                load_character(def->character);
                break;
            default:
                ostringstream msg;
                msg << "No handler for definition type " << def->type;
                throw model_exception(msg.str().c_str());
        }
    }
}

void model::load_skilllist(skill_t *ptrSk) {
    auto sk = std::make_shared<skill>(ptrSk);
    skills[sk->name] = sk;
}

void model::load_talenlist(talent_t *tlnt) {
    auto tl = std::make_shared<talent>(tlnt);
    talents[tl->name] = tl;
}

character_parts& character_parts::operator+=(const character_parts& rhs) {
    attributes.insert(attributes.end(), rhs.attributes.begin(), rhs.attributes.end());
    for(auto p : rhs.skills) {
        if (skills.contains(p.first)) skills[p.first] += p.second;
        else skills[p.first] = p.second;
    }
    for(auto p : rhs.talents) {
        if (talents.contains(p.first)) talents[p.first] += p.second;
        else talents[p.first] = p.second;
    }
    weapons.insert(rhs.weapons.begin(), rhs.weapons.end());
    return *this;
}


void model::itemList_to_character_parts(vector<item_t*> items, character_parts& parts) {
    for(auto item : items) {
        switch(item->type) {
            case item_attributBonus:
                parts.attributes.push_back(make_shared<attribute_bonus>(attribute_bonus(*item->bonus)));
                break;
            case item_leveledItem:
                if (talents.contains(item->item->name)) {
                    auto sp = get_talent_reference(item->item->name);
                    sp.level = item->item->level;
                    if (parts.talents.contains(item->item->name)) parts.talents[item->item->name] += *item->item;
                    else parts.talents[item->item->name] = sp;
                    break;
                } else if (skills.contains(item->item->name)) {
                    auto sp = get_skill_reference(item->item->name);
                    sp.proficiency = item->item->level ? item->item->level : 1;
                    if (parts.skills.contains(item->item->name)) parts.skills[item->item->name] += sp;
                    else parts.skills[item->item->name] = sp;
                    break;
                } else if (packages.contains(item->item->name)) {
                    auto p = packages[item->item->name];
                    parts += *p;
                    break;
                }
                else {
                    ostringstream msg;
                    msg << "Unknown item " << item->item->name;
                    throw model_exception(msg.str().c_str());
                }
            default: {
                ostringstream msg;
                msg << "Unknown item " << item->type;
                throw model_exception(msg.str().c_str());
            }
        }
    }
}

void load_weapon(weapon_t*) {

}

void model::load_package(package_t *pkg) {
    auto p = std::make_shared<package>(pkg);
    itemList_to_character_parts(pkg->items, *p);
    packages[p->name] = p;
}

void model::load_character(character_t *ch) {

    auto thischar = std::make_shared<character>(ch);
    itemList_to_character_parts(ch->items, *thischar);
    for(auto ab : thischar->attributes) {
        thischar->attribute[ab->attribute] += ab->bonus;
    }
    characters[thischar->name] = thischar;
}

void model::add_skill(string name, attribute_t attribute) {
    skills[name] = make_shared<skill>(name, attribute);
}

void model::add_talent(string name) {
    talents[name] = make_shared<talent>(name);
}

void model::add_weapon(weapon_t &w) {
    weapons[w.name] = make_shared<weapon_t>(w);
}

skill_reference model::get_skill_reference(string name) {
    auto sk = skills[name];
    auto sr = skill_reference(sk, 0);
    return sr;
}

talent_reference model::get_talent_reference(string name) {
    if (!talents.contains(name)) {
        ostringstream msg;
        msg << "Talent \"" << name << "\" not found in world.";
        throw model_exception(msg.str().c_str());
    }
    auto tl = talents[name];
    auto tr = talent_reference(tl, 0);
    return tr;
}


ostream &operator<<(ostream &out, character &c) {
    if (!out) return out;

    out << c.name << endl;
    out << endl;
    out << "Br Ag Int Cun Will Pr" << endl
        << "-- -- --- --- ---- --" << endl;
    out << setw(2) << c.attribute[attr_brawn] << " "
        << setw(2) << c.attribute[attr_agility] << " "
        << setw(3) << c.attribute[attr_intellect] << " "
        << setw(3) << c.attribute[attr_cunning] << " "
        << setw(4) << c.attribute[attr_willpower] << " "
        << setw(2) << c.attribute[attr_presence] << endl << endl;

    if (!c.talents.empty()) {
        bool first = true;
        for (auto p: c.talents) {
            if (first) {
                first = false;
            } else {
                out << ", ";
            }
            out << p.second;
        }
        out << endl;
    }

    if (!c.skills.empty()) {
        out << endl;
        int length = 0;
        string name;
        for (auto p: c.skills) {
            ostringstream measure;
            measure << *(p.second.parent);
            string text = measure.str();
            if (text.length() > length) length = text.length();
        }
        for (auto p: c.skills) {
            name = p.second.parent->name;
            ostringstream text;
            text << *(p.second.parent);
            out << setw(length) << left << text.str() << "  " << c.proficiency(name) << "/" << c.ability(name) << endl;
        }
    }

    if (!c.weapons.empty()) {
        int namelength = 0;
        int skilllength = 0;
        for(auto p : c.weapons) {
            ostringstream nametext;
            ostringstream skilltext;
            nametext << p.second->name;
            skilltext << p.second->skill;
            if (nametext.str().length() > namelength) namelength = nametext.str().length();
            if (skilltext.str().length() > skilllength) skilllength = skilltext.str().length();
        }

        out << endl;
        for(auto p : c.weapons) {
            char brawlmarker = p.second->brawl ? '+' : ' ';
            out << setw(namelength) << left << p.second->name << "  "
                << setw(skilllength) << left << p.second->skill << "  "
                << "Damage " << brawlmarker << p.second->damage << "  "
                << "Crit " << p.second->crit << endl;
        }

    }

    return out;
}

model_exception::model_exception(const char* m) {
    message = strdup(m);
}

const char *model_exception::what() const noexcept {
    return message;
}

ostream& operator<<(ostream& out, model_exception& me) {
    if (!out) return out;
    out << "model exception: " << me.what();
    return out;
}
