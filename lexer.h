#ifndef _LEXER_H_
#define _LEXER_H_

#include <iostream>
#include <string>
using std::string;
#include <map>
using std::map;

#include "token.h"


class lexer {

private:
    std::istream& src;
    unsigned int lineno;
    token* hold;
    bool holding;

    bool string_match(string, string);
    token get_word(const char);
    token get_number(const char);
    token get_quoted_string(std::istream&);

    enum attribute_t to_attribute(const char*);

    map<string, token_type> keyword;

public:

    lexer(std::istream&);
    token get();
    void put_back(token&);
    bool eof();
    unsigned int get_line();
};

#endif
