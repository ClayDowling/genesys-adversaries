#ifndef PARSEEXCEPTION_H
#define PARSEEXCEPTION_H

#include <exception>
#include <iostream>

using std::exception;
using std::ostream;

class parse_exception : public std::exception {
private:
    const char* message;
    int lineno;

public:

    parse_exception(const char*, int);
    parse_exception(const char*);

    virtual const char* what() const noexcept;

};

ostream& operator<<(ostream&, parse_exception&);

#endif