#include "lexer.h"
#include <cctype>
#include <cstring>
#include <sstream>
using std::istringstream;

token::token() : type(EndOfInput), line(0), value(""), attribute(attr_MAX) {}
token::token(enum token_type t, unsigned int l) : type(t), line(l), attribute(attr_MAX) {}
token::token(enum token_type t, unsigned int l, string v) : type(t), line(l), value(v), attribute(attr_MAX) {}
token::token(enum token_type t, unsigned int l, string v, attribute_t a) : type(t), line(l), value(v), attribute(a) {}

// Copy constructor
token::token(const token& rhs) : type(rhs.type), line(rhs.line), value(rhs.value), attribute(rhs.attribute) {}

lexer::lexer(std::istream& in) : src(in), lineno(1), holding(false), hold(nullptr),
    keyword({
                    {"skill",    SKILL},
                    {"skills",   SKILL},
                    {"pack",     PACKAGE},
                    {"package",  PACKAGE},
                    {"tal",      TALENT},
                    {"talent",   TALENT},
                    {"talents",  TALENT},
                    {"rival",    CHARACTER},
                    {"minion",   CHARACTER},
                    {"nemesis",  CHARACTER},
                    {"use",      USE},
                    {"weapon",   WEAPON},
                    {"damage",   DAMAGE},
                    {"dmg",      DAMAGE},
                    {"range",    RANGE},
                    {"critical", CRITICAL},
                    {"crit",     CRITICAL},
                    {"engaged",  ENGAGED},
                    {"short",    SHORT},
                    {"close",    SHORT},
                    {"medium",   MEDIUM},
                    {"long",     LONG},
                    {"extreme",  EXTREME},
                    {"encumbrance", ENCUMBRANCE},
                    {"enc", ENCUMBRANCE}
            })
{}

unsigned int lexer::get_line() {
    return lineno;
}

token lexer::get() {

    if (holding) {
        holding = false;
        token t(*hold);
        delete hold;
        return t;
    }

    if (!src) {
        return token(EndOfInput, lineno);
    }

    if (src.eof()) {
        return token(EndOfInput, lineno);
    }

    char c = src.get();

    if (EOF == c) {
        return token(EndOfInput, lineno);
    }

    while(c == ' ' || c == '\t' || c == '\r' || c == '\v') c = src.get();
    if (isdigit(c) || c == '-' || c == '+') {
        return get_number(c);
    }
    if (isalpha(c)) {
        auto n = get_word(c);

        for(auto kw: keyword) {
            if (string_match(n.value, kw.first)) return token(kw.second, n.line, n.value);
        }

        auto attribute = to_attribute(n.value.c_str());
        if (attribute != attr_MAX) {
            return token(ATTRIBUTE, lineno, n.value, attribute);
        }

        return n;
    }
    switch(c) {
        case '(':
            return token(LPAREN, lineno, "(");
        case ')':
            return token(RPAREN, lineno, ")");
        case '\n':
            ++lineno;
            return token(NEWLINE, lineno);
        case ',':
            return token(COMMA, lineno, ",");
        case ';':
            return token(SEMICOLON, lineno, ";");
        case ':':
            return token(COLON, lineno, ":");
        case '\"':
            return get_quoted_string(src);
        default:
            return token(UNKNOWN, lineno);
    }
}

token lexer::get_word (const char firstletter) {
    std::string name;
    name += firstletter;

    char nc;
    
    while((nc = src.get()) != EOF) {
        if (isalnum(nc)) {
            name += nc;
        } else if (nc == '-') {
            name += nc;
        }
        else {
            src.putback(nc);
            break;
        }
    }

    name.erase(name.find_last_not_of(" \t") + 1);

    return token(WORD, lineno, name);
}

token lexer::get_number(const char firstletter) {
    std::string number;
    number += firstletter;

    char nc;
    while((nc = src.get()) != EOF) {
        if (std::isdigit(nc)) {
            number += nc;
        } else {
            src.putback(nc);
            break;
        }
    }

    return token(NUMBER, lineno, number);
}

token lexer::get_quoted_string(std::istream& src) {
    string value;

    char c = src.get();
    while(src && c != '\"') {
        value += c;
        c = src.get();
    }
    return token(QUOTEDSTRING, lineno, value);
}

bool lexer::eof() {
    return src.eof();    
}

void lexer::put_back(token& t) {
    holding = true;
    hold = new token(t);
}

enum attribute_t lexer::to_attribute(const char* text) {

    attribute_t a;
    istringstream in(text);
    in >> a;

    return a;
}

bool lexer::string_match(string lhs, string rhs) {
    return strcasecmp(lhs.c_str(), rhs.c_str()) == 0;
}

