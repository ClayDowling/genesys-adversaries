//
// Created by Clay on 5/31/2022.
//
#include "token.h"

ostream &operator<<(ostream &out, enum token_type v) {
    if (!out) return out;

    switch (v) {
        case UNKNOWN:
            out << "UNKNOWN";
            break;
        case WORD:
            out << "WORD";
            break;
        case NUMBER:
            out << "NUMBER";
            break;
        case LPAREN:
            out << "LPAREN";
            break;
        case RPAREN:
            out << "RPAREN";
            break;
        case CHARACTER:
            out << "CHARACTER";
            break;
        case COMMA:
            out << "COMMA";
            break;
        case SEMICOLON:
            out << "SEMICOLON";
            break;
        case COLON:
            out << "COLON";
            break;
        case NEWLINE:
            out << "NEWLINE";
            break;
        case ATTRIBUTE:
            out << "ATTRIBUTE";
            break;
        case SKILL:
            out << "SKILL";
            break;
        case PACKAGE:
            out << "PACKAGE";
            break;
        case TALENT:
            out << "TALENT";
            break;
        case QUOTEDSTRING:
            out << "QUOTEDSTRING";
            break;
        case USE:
            out << "USE";
            break;
        case WEAPON:
            out << "WEAPON";
            break;
        case ENGAGED:
            out << "ENGAGED";
            break;
        case SHORT:
            out << "SHORT";
            break;
        case MEDIUM:
            out << "MEDIUM";
            break;
        case LONG:
            out << "LONG";
            break;
        case EXTREME:
            out << "EXTREME";
            break;
        case DAMAGE:
            out << "DAMAGE";
            break;
        case CRITICAL:
            out << "CRITICAL";
            break;
        case RANGE:
            out << "RANGE";
            break;
        case ENCUMBRANCE:
            out << "ENCUMBRANCE";
            break;
        case EndOfInput:
            out << "EndOfInput";
            break;
        default:
            out << v;
            break;
    }
    return out;
}

