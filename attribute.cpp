//
// Created by Clay on 4/9/2022.
//

#include <string>
using std::string;
#include <cstring>

#include "attribute.h"

const char* attribute_name[attr_MAX] = {"Brawn", "Agility", "Intellect", "Cunning", "Willpower", "Presence"};
const char* attribute_abbrv[attr_MAX] = {"BR", "AG", "INT", "CUN", "WILL", "PR"};


ostream& operator<<(ostream& out, const attribute_t& a)
{
    if (out) {
        if (a < attr_MAX && a >= 0) {
            out << attribute_name[a];
        } else {
            out << "UNKNOWN";
        }
    }
    return out;
}

istream& operator>>(istream& in, attribute_t& a) {
    a = attr_MAX;
    if (in) {
        string word;
        in >> word;
        for(int i=0; i < attr_MAX; ++i) {
            if (strcasecmp(word.c_str(), attribute_name[i]) == 0) {
                a = (attribute_t)i;
                break;
            }
            if (strcasecmp(word.c_str(), attribute_abbrv[i]) == 0) {
                a = (attribute_t)i;
                break;
            }
        }
    }
    return in;
}