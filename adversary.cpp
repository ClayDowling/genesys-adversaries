#include "lexer.h"
#include "model.h"

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;
#include <fstream>
using std::ifstream;

#include "lexer.h"
#include "parser.h"
#include "parseexception.h"

using namespace std;

int main(int argc, const char** argv) {

	if (1 == argc) {
		cerr << "usage: " << argv[0] << " myenemies.adv" << endl;
		return 1;
	}

	try {
		ifstream in(argv[1]);
		lexer src(in);
		parser p;
		auto ast = p.parse(src);
		in.close();

        model world(ast);

        for(auto p : world.characters) {
            cout << p.second << endl;
        }

	} catch(parse_exception& e) {
		cerr << e << endl;
	} catch(model_exception& e) {
        cerr << e << endl;
    }

	return 0;
}
