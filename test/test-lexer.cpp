#include "lexer.h"
#include <catch2/catch_test_macros.hpp>

#include <sstream>
using std::istringstream;


const char* LEXER_TAG = "[lexer]";

TEST_CASE("Given parens, lexer returns paren tokens", LEXER_TAG) {
    istringstream in("()");

    lexer luthor(in);

    auto first = luthor.get();
    auto second = luthor.get();

    REQUIRE( first.type == LPAREN );
    REQUIRE( first.line == 1 );

    REQUIRE( second.type == RPAREN );
    REQUIRE( second.line == 1 );

}

TEST_CASE("Given parens on different lines, lexer returns expected line number", LEXER_TAG) {
    istringstream in("(\n)");

    lexer luthor(in);

    auto first = luthor.get();
    auto nl = luthor.get();
    auto second = luthor.get();

    REQUIRE( first.type == LPAREN );
    REQUIRE( first.line == 1 );

    REQUIRE( nl.type == NEWLINE );

    REQUIRE( second.type == RPAREN );
    REQUIRE( second.line == 2 );

}

TEST_CASE("Given a comma, lexter returns COMMA", LEXER_TAG) {
    istringstream in(",");
    lexer luthor(in);

    auto t = luthor.get();
    REQUIRE( t.type == COMMA );
}

TEST_CASE("Given a semicolon, lexer returns SEMICOLON", LEXER_TAG) {
    istringstream in (";");
    lexer luthor(in);

    auto t = luthor.get();
    REQUIRE( t.type == SEMICOLON );
}

TEST_CASE("Given a colon, lexer returns COLON", LEXER_TAG) {
    istringstream in (":");
    lexer luthor(in);

    auto t = luthor.get();
    REQUIRE( t.type == COLON );
}

TEST_CASE("Given word, lexer returns NAME symbol", LEXER_TAG) {
    istringstream in("Word");

    lexer luthor(in);

    auto w = luthor.get();

    REQUIRE( w.type == WORD );
    REQUIRE( w.value == "Word" );
    REQUIRE( w.line == 1 );
}

TEST_CASE("Lexer skips whitespace", LEXER_TAG) {
    istringstream in (" \t\n");

    lexer luthor(in);

    auto t = luthor.get();

    REQUIRE( t.type == NEWLINE );
}

TEST_CASE("Parsing words, lexer does not consume extra characters", LEXER_TAG) {
    istringstream in ("Wordly\n");

    lexer luthor(in);

    auto w = luthor.get();
    auto nl = luthor.get();

    REQUIRE( w.type == WORD );
    REQUIRE( w.value == "Wordly" );

    REQUIRE( nl.type == NEWLINE);
}

TEST_CASE("Given multiple words, lexer will return all words as a single name", LEXER_TAG) {
    istringstream in ("First Second Third \t");

    lexer luthor(in);

    auto first = luthor.get();
    auto second = luthor.get();
    auto third = luthor.get();

    REQUIRE( first.type == WORD );
    REQUIRE( first.value == "First" );
    REQUIRE( second.type == WORD );
    REQUIRE( second.value == "Second" );
    REQUIRE( third.type == WORD );
    REQUIRE( third.value == "Third" );
}

TEST_CASE("Given word with number, lexer returns a single word with the number", LEXER_TAG) {
    istringstream in ("Colt-45");

    lexer luthor(in);

    auto smoothtaste = luthor.get();

    REQUIRE( smoothtaste.type == WORD );
    REQUIRE( smoothtaste.value == "Colt-45" );
}

TEST_CASE("Given hypenated word, lexer returns word as single token", LEXER_TAG) {
    istringstream in("Smith-Jones");

    lexer luthor(in);

    auto smithjones = luthor.get();

    REQUIRE( smithjones.type == WORD );
    REQUIRE( smithjones.value == "Smith-Jones" );
}

TEST_CASE("When input is exhausted, lexer returns END", LEXER_TAG) {
    istringstream in("Text");

    lexer luthor(in);

    auto word = luthor.get();
    auto end = luthor.get();
    auto noreally = luthor.get();

    REQUIRE( word.value == "Text" );
    REQUIRE( end.type == EndOfInput );
    REQUIRE( noreally.type == EndOfInput );
}

TEST_CASE("When input still exists, eof returns false", LEXER_TAG) {
    istringstream in("Text,");

    lexer luthor(in);

    auto word = luthor.get();

    REQUIRE( luthor.eof() == false );
}

TEST_CASE("When input is exhausted, eof returns false", LEXER_TAG) {
    istringstream in("");

    lexer luthor(in);

    auto word = luthor.get();

    REQUIRE( word.type == EndOfInput );
    REQUIRE( luthor.eof() == true );
}

TEST_CASE("When a token is put back, it is the first token read again", LEXER_TAG) {
    istringstream in("one:");
    lexer luthor(in);

    auto first = luthor.get();
    luthor.put_back(first);

    auto second = luthor.get();
    REQUIRE( second.value == "one" );
    
}

TEST_CASE("Lexer can recognize attributes", LEXER_TAG) {
    istringstream in("Brawn AgIlItY Intellect Cunning WillPOWER presence");
    lexer luthor(in);

    auto brawn = luthor.get();
    auto agility = luthor.get();
    auto intellect = luthor.get();
    auto cunning = luthor.get();
    auto willpower = luthor.get();
    auto presence = luthor.get();

    REQUIRE( brawn.type == ATTRIBUTE );
    REQUIRE( brawn.attribute == attr_brawn );
    REQUIRE( agility.type == ATTRIBUTE );
    REQUIRE( agility.attribute == attr_agility );
    REQUIRE( intellect.type == ATTRIBUTE );
    REQUIRE( intellect.attribute == attr_intellect );
    REQUIRE( cunning.type == ATTRIBUTE );
    REQUIRE( cunning.attribute == attr_cunning );
    REQUIRE( willpower.type == ATTRIBUTE );
    REQUIRE( willpower.attribute == attr_willpower );
    REQUIRE( presence.type == ATTRIBUTE );
    REQUIRE( presence.attribute == attr_presence );

}

TEST_CASE("Lexer recognizes keywords", LEXER_TAG) {
    istringstream in("skill skills pack package talent tal");
    lexer luthor(in);

    auto skill1 = luthor.get();
    auto skill2 = luthor.get();
    auto pack1 = luthor.get();
    auto pack2 = luthor.get();
    auto tal1 = luthor.get();
    auto tal2 = luthor.get();

    REQUIRE(skill1.type == SKILL);
    REQUIRE(skill2.type == SKILL);
    REQUIRE(pack1.type == PACKAGE);
    REQUIRE(pack2.type == PACKAGE);
    REQUIRE(tal1.type == TALENT);
    REQUIRE(tal2.type == TALENT);

}

TEST_CASE("Lexer recognizes integers without sign", LEXER_TAG) {
    istringstream in("123");
    lexer luthor(in);

    auto num = luthor.get();

    REQUIRE( num.type == NUMBER );
    REQUIRE( num.value == "123" );
}


TEST_CASE("Lexer recognizes quoted string", LEXER_TAG) {
    istringstream in("\"/this/is/a long name\"");
    lexer luthor(in);

    auto qs = luthor.get();
    REQUIRE( qs.type == QUOTEDSTRING );
    REQUIRE( qs.value == "/this/is/a long name" );
}

TEST_CASE("Lexer can recognize USE keyword", LEXER_TAG) {
    istringstream in("use \"core\"");
    lexer luthor(in);

    auto u = luthor.get();
    auto qs = luthor.get();

    REQUIRE( u.type == USE );
    REQUIRE( qs.type == QUOTEDSTRING );
}

void testKeywordVariant(string keywords, token_type expected) {
    istringstream in(keywords);
    lexer luthor(in);
    for(auto tok = luthor.get(); tok.type != EndOfInput; tok = luthor.get()) {
        REQUIRE( tok.type == expected );
    }
}

TEST_CASE("Lexer recognizes CHARACTER variants", LEXER_TAG) {
    testKeywordVariant("MINION minion RIVAL rival NEMESIS nemesis NeMeSiS", CHARACTER);
}

TEST_CASE("Lexer recognized WEAPON keyword", LEXER_TAG) {
    testKeywordVariant("Weapon WEAPON weapon wEaPoN", WEAPON);
}

TEST_CASE("Lexer recognizes DAMAGE tokens", LEXER_TAG) {
    testKeywordVariant("Damage damage DAMAGE dmg Dmg DMG", DAMAGE);
}

TEST_CASE("Lexer recognizes RANGE tokens", LEXER_TAG) {
    testKeywordVariant("range RANGE rAnGe", RANGE);
}

TEST_CASE("Lexer recognizes CRITICAL tokens", LEXER_TAG) {
    testKeywordVariant("Critical CRITICAL critical crit CRIT", CRITICAL);
}

TEST_CASE("Lexer recognizes ENGAGED tokens", LEXER_TAG) {
    testKeywordVariant("engaged ENGAGED enGagEd", ENGAGED);
}

TEST_CASE("Lexer recognizes SHORT tokens", LEXER_TAG) {
    testKeywordVariant("short SHORT ShOrT close CLOSE cLoSe", SHORT);
}

TEST_CASE("Lexer recognizes MEDIUM tokens", LEXER_TAG) {
    testKeywordVariant("medium MEDIUM MedIuM", MEDIUM);
}

TEST_CASE("Lexer recognizes LONG tokens", LEXER_TAG) {
    testKeywordVariant("long LONG LoNg", LONG);
}

TEST_CASE("Lexer recognizes EXTREME tokens", LEXER_TAG) {
    testKeywordVariant("extreme EXTREME exTrEme", EXTREME);
}

TEST_CASE("Lexer recognizes ENCUMBRANCE tokens", LEXER_TAG) {
    testKeywordVariant("encumbrance ENCUMBRANCE Enc ENC", ENCUMBRANCE);
}
