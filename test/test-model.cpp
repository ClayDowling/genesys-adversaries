#include "model.h"
#include <catch2/catch_test_macros.hpp>

#include <sstream>
using std::ostringstream;
#include <memory>
using std::shared_ptr;
using std::make_shared;
#include <algorithm>
using std::find_if;

#include "testsupport.h"
#include "parser.h"


const char* MODEL_TAG = "[model]";


TEST_CASE("Positive Attribute Bonus Displays With Plus Sign", MODEL_TAG) {
    attributebonus_t src;
    src.attribute = attr_cunning;
    src.level = 2;

    attribute_bonus ab(src);

    ostringstream text;
    text << ab;

    REQUIRE( text.str() == "Cunning +2");
}

TEST_CASE("Skill from skill_t writes name and attribute", MODEL_TAG) {
    skill_t* s = new skill_t();
    s->name = "Guns";
    s->attribute = attr_agility;

    skill actual(s);

    ostringstream text;
    text << actual;

    REQUIRE( text.str() == "Guns (Agility)" );
}

TEST_CASE("Skill_reference with associated skill outputs skill", MODEL_TAG) {
    auto sk = std::make_shared<skill>("Cool", attr_presence);
    skill_reference actual (sk, 2);

    ostringstream text;
    text << actual;

    REQUIRE( text.str() == "Cool (Presence): 2" );
}

TEST_CASE("Talent constructed from talent_t outputs name of talent", MODEL_TAG) {
    talent_t t;
    t.name = "Divination";

    talent actual(&t);
    ostringstream text;
    text << actual;

    REQUIRE( text.str() == "Divination" );
}

TEST_CASE("talent_reference with talent and level prints talent and level", MODEL_TAG) {
    auto t = std::make_shared<talent>("Divination");
    talent_reference tr(t, 3);

    ostringstream text;
    text << tr;

    REQUIRE( text.str() == "Divination 3" );
}

TEST_CASE("talent_reference with talent and no level prints talent without level", MODEL_TAG) {
    auto t = std::make_shared<talent>("Born With It");
    talent_reference tr(t, 0);

    ostringstream text;
    text << tr;

    REQUIRE( text.str() == "Born With It" );
}

bool hasSkill(vector<shared_ptr<skill> >& list, string name) {
    return find_if(list.begin(), list.end(), identifySkill(name)) != list.end();
}

TEST_CASE("add_skill puts skill into model", MODEL_TAG) {
    model m;
    m.add_skill("Driving", attr_agility);

    REQUIRE( m.skills.contains("Driving"));

    auto s = m.skills["Driving"];
    REQUIRE(s->name == "Driving");
    REQUIRE(s->attribute == attr_agility);
}

TEST_CASE("add_talent puts talent into model", MODEL_TAG) {
    model m;
    m.add_talent("Noble Birth");

    REQUIRE( m.talents.contains("Noble Birth"));

    auto t = m.talents["Noble Birth"];
    REQUIRE( t->name == "Noble Birth" );
}

TEST_CASE("get_talent_reference returns reference to named talent when it is present", MODEL_TAG) {
    model m;
    m.add_talent("Secret Identity");

    auto tr = m.get_talent_reference("Secret Identity");

    REQUIRE( tr.parent != nullptr );
    REQUIRE( tr.parent->name == "Secret Identity");
}

TEST_CASE("get_talent_reference throws parse exception when adding a talent to a character that doesn't exist", MODEL_TAG) {
    model m;
    m.add_talent("Secret Identity");

    REQUIRE_THROWS_AS(m.get_talent_reference("Famous"), model_exception );
}

TEST_CASE("add_weapon puts weapon into model", MODEL_TAG) {
    model m;
    weapon dagger;
    dagger.name = "Dagger";
    dagger.crit = 4;
    dagger.damage = 2;
    m.add_weapon(dagger);

    REQUIRE(m.weapons.contains("Dagger"));

    auto actual = m.weapons["Dagger"];
    REQUIRE( actual->damage == 2 );
    REQUIRE( actual->crit == 4 );
    REQUIRE( actual->name == "Dagger" );
}

TEST_CASE("Model populated with definition list containing skills will put skills in list", MODEL_TAG) {
    auto ast = parseInput("skill Trombone (Cunning)\n"
                          "skill Athletics (Brawn)\n"
                          "skill Driving (Agility)"
            );
    model m(ast);

    REQUIRE( m.skills.size() == 3 );

    REQUIRE( m.skills.contains("Trombone") );
    REQUIRE( m.skills.contains("Athletics") );
    REQUIRE( m.skills.contains("Driving") );

}

TEST_CASE("Model populated with definition list contains talens will put talents in list", MODEL_TAG) {
    auto ast = parseInput("talent Power Drinking\n"
                          "talent Invisible\n"
            );
    model m(ast);

    REQUIRE( m.talents.size() == 2 );
    REQUIRE( m.talents.contains("Power Drinking") );
    REQUIRE( m.talents.contains("Invisible") );
}

TEST_CASE("Lookup for skill name in list returns skill", MODEL_TAG) {
    auto ast = parseInput("skill Guns (Ag)\n");
    model m(ast);

    auto actual = m.skills["Guns"];

    REQUIRE( nullptr != actual );
    REQUIRE( actual->attribute == attr_agility );
    REQUIRE( actual->name == "Guns" );
}

TEST_CASE("Package populated from package_t contains all members", MODEL_TAG) {
    auto ast = parseInput(
            "skill Guns (Ag), Knife (Ag)\n"
            "talent Adversary, Born To It\n"
            "package sample: Br +1, Ag +2, Born To It, Adversary 1, Guns 2, Knife 1");

    model m(ast);

    REQUIRE( m.packages.size() == 1 );

    auto p = m.packages["sample"];

    REQUIRE( p != nullptr );

    ostringstream text;
    text << *p;

    REQUIRE( text.str() == string("sample: Brawn +1, Agility +2, Adversary 1, Born To It, Guns (Agility): 2, Knife (Agility): 1") );

}


TEST_CASE("Character populated from packages contains all members of packages", MODEL_TAG) {
    auto ast = parseInput("skill Driving (Agility)\n"
                          "skill Brawling (Brawn)\n"
                          "talent Adversary\n"
                          "package Tough Guy: Br +3, Ag +2, Int +1\n"
                          "package Gang Heavy: Brawling, Adversary, Driving\n"
                          "rival Bruiser: Tough Guy, Gang Heavy");

    model m(ast);

    REQUIRE( m.characters.size() == 1);

    auto c = m.characters["Bruiser"];
    REQUIRE( c->skills.contains("Brawling") );
    REQUIRE( c->skills.contains("Driving") );
    REQUIRE( c->talents.contains("Adversary") );
    REQUIRE( c->attribute[attribute_t::attr_brawn] == 3 );
    REQUIRE( c->attribute[attribute_t::attr_agility] == 2 );
    REQUIRE( c->attribute[attribute_t::attr_intellect] == 1 );
    REQUIRE( c->attribute[attribute_t::attr_presence] == 0 );

}

TEST_CASE("Character skills add correctly.", MODEL_TAG) {
    auto ast = parseInput("skill Driving (Agility)\n"
                          "skill Brawling (Brawn)\n"
                          "talent Adversary\n"
                          "package Tough Guy: Br +3, Ag +2, Int +1\n"
                          "package Gang Heavy: Brawling, Adversary, Driving\n"
                          "rival Ruffian: Tough Guy, Brawling, Gang Heavy");
    model m(ast);
    auto c = m.characters["Ruffian"];
    auto s = c->skills["Brawling"];
    REQUIRE(s.proficiency == 2);
}

TEST_CASE("Character leveled talents add properly.", MODEL_TAG) {
    auto ast = parseInput("skill Driving (Agility)\n"
                          "skill Brawling (Brawn)\n"
                          "talent Adversary\n"
                          "package Tough Guy: Br +3, Ag +2, Int +1\n"
                          "package Gang Heavy: Brawling, Adversary 1, Driving\n"
                          "rival Brute: Tough Guy, Adversary 1, Gang Heavy");
    model m(ast);
    auto c = m.characters["Brute"];
    auto t = c->talents["Adversary"];
    REQUIRE(t.level == 2);
}

TEST_CASE("skill_reference returns pointer to an existing skill", MODEL_TAG) {
    model m;
    m.add_skill("Driving", attr_agility);
    auto sr = m.get_skill_reference("Driving");

    REQUIRE( sr.parent->name == "Driving" );
    REQUIRE( sr.parent->attribute == attr_agility );
}

TEST_CASE("Weapon output - Minimal", MODEL_TAG) {
    weapon w;
    w.name = "Club";
    w.brawl = true;
    w.damage = 2;
    w.crit = 4;
    w.skill = "Melee";

    ostringstream out;
    out << w;
    REQUIRE( out.str() == "Club (Melee; Damage +2; Crit 4)");
}

TEST_CASE("Weapon output with Specials", MODEL_TAG) {

        weapon w;
        w.name = "Small Pistol";
        w.brawl = false;
        w.damage = 3;
        w.crit = 2;
        w.skill = "Ranged-Light";

        w.special.push_back("Accuracy 2");
        w.special.push_back("Customized");

        ostringstream out;
        out << w;
        REQUIRE( out.str() == "Small Pistol (Ranged-Light; Damage 3; Crit 2; Accuracy 2, Customized)");
}
