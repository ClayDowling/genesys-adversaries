//
// Created by clay on 6/14/22.
//
#include <memory>
using std::make_shared;
#include <sstream>
using std::ostringstream;

#include <catch2/catch_test_macros.hpp>
#include "model.h"

SCENARIO("Calculating Dice Pools for a Character In a World") {
    GIVEN("Bobby the Wheel Man") {

        model world;
        world.add_skill("Driving", attr_agility);
        world.add_skill("Running", attr_brawn);

        character bobby;
        bobby.name = "Bobby the Wheel Man";
        bobby.attribute[attr_brawn] = 2;
        bobby.attribute[attr_agility] = 4;

        auto drive = world.get_skill_reference("Driving");
        drive.proficiency = 3;
        bobby.skills["Driving"] = drive;

        auto run = world.get_skill_reference("Running");
        bobby.skills["Running"] = run;

        WHEN("Proficiency is calculated") {
            THEN("Driving Proficiency is equal to skill proficiency") {
                REQUIRE( bobby.proficiency("Driving") == 3 );
            }
            AND_THEN("Running Proficiency defaults to zero") {
                REQUIRE( bobby.proficiency("Running") == 0 );
            }
        }

        WHEN("Abilities are calculated") {
            THEN("Running ability defaults to running attribute") {
                REQUIRE( bobby.ability("Running") == bobby.attribute[attr_brawn] );
            }
            THEN("Driving proficiency plus driving ability equals driving attribute") {
                REQUIRE( bobby.ability("Driving") == 1 );
            }
        }

    }
}

SCENARIO("Creating text output of a character") {
    GIVEN("A character with weapons") {
        model world;

        world.add_skill("Melee", attr_agility);
        world.add_skill("Ranged (Light)", attr_agility);
        world.add_skill("Brawling", attr_brawn);
        world.add_talent("Made Man");
        world.add_talent("Bloodthirsty");

        character cathy;
        cathy.name = "Catherine The Bloody";
        cathy.attribute[attr_brawn] = 3;
        cathy.attribute[attr_agility] = 2;

        auto melee = world.get_skill_reference("Melee");
        melee.proficiency = 2;
        auto brawl = world.get_skill_reference("Brawling");
        brawl.proficiency = 3;
        auto guns = world.get_skill_reference("Ranged (Light)");
        guns.proficiency = 1;

        cathy.skills["Melee"] = melee;
        cathy.skills["Brawling"] = brawl;
        cathy.skills["Ranged (Light)"] = guns;

        weapon_t knife;
        knife.name = "Knife";
        knife.skill = "Melee";
        knife.damage = 1;
        knife.crit = 3;
        knife.brawl = true;

        weapon_t gun;
        gun.name = "Small Gun";
        gun.damage = 3;
        gun.crit = 3;
        gun.skill = "Ranged (Light)";

        cathy.weapons["Small Gun"] = make_shared<weapon_t>(gun);
        cathy.weapons["Knife"] = make_shared<weapon_t>(knife);

        auto mademan = world.get_talent_reference("Made Man");
        mademan.level = 1;
        auto bloodthirsty = world.get_talent_reference("Bloodthirsty");

        cathy.talents["Made Man"] = mademan;
        cathy.talents["Bloodthirsty"] = bloodthirsty;

        WHEN("Displayed as plain text") {
            ostringstream text;
            text << cathy;
            THEN("Expect neatly formatted output") {
                string expected = "Catherine The Bloody\n"
                                  "\n"
                                  "Br Ag Int Cun Will Pr\n"
                                  "-- -- --- --- ---- --\n"
                                  " 3  2   0   0    0  0\n"
                                  "\n"
                                  "Bloodthirsty, Made Man 1\n"
                                  "\n"
                                  "Brawling (Brawn)          3/0\n"
                                  "Melee (Agility)           2/0\n"
                                  "Ranged (Light) (Agility)  1/1\n"
                                  "\n"
                                  "Knife      Melee           Damage +1  Crit 3\n"
                                  "Small Gun  Ranged (Light)  Damage  3  Crit 3\n";

                REQUIRE( text.str() == expected );
            }
        }

        WHEN("Displayed in short form") {
            ostringstream text;
            cathy.shortform(text);
            string expected = "Catherine The Bloody\n"
                              "\n"
                              "Br Ag Int Cun Will Pr\n"
                              "-- -- --- --- ---- --\n"
                              " 3  2   0   0    0  0\n"
                              "\n"
                              "Bloodthirsty, Made Man 1\n"
                              "Brawling (Brawn): 3, Melee (Agility): 2, Ranged (Light) (Agility): 1\n"
                              "Knife (Melee; Damage +1; Crit 3)\n"
                              "Small Gun (Ranged (Light); Damage 3; Crit 3)\n";

            REQUIRE( text.str() == expected );
        }

    }
}
