//
// Created by Clay on 4/15/2022.
//

#ifndef ADVERSARIES_TESTSUPPORT_H
#define ADVERSARIES_TESTSUPPORT_H

#include "ast.h"

definitionList_t parseInput(const char* input);

#endif //ADVERSARIES_TESTSUPPORT_H
