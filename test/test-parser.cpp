#include "parser.h"
#include "parseexception.h"
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include "lexer.h"
#include <unistd.h>

#include <iostream>
using std::cerr;
using std::endl;
#include <sstream>
using std::istringstream;
using std::ostringstream;
#include <fstream>
using std::ofstream;
#include <exception>
using std::exception;
#include <cstdio>
using std::tmpnam;
using std::remove;
#include <algorithm>
using std::find_if;

const char* PARSER_TAG = "[parser]";

using namespace Catch::Matchers;

definitionList_t parseInput(const char* input) {
    istringstream in(input);
    lexer lex(in);
    parser p;

    return p.parse(lex);
}

TEST_CASE("On EndOfInput with no content, returns empty ast", PARSER_TAG) {
    auto ast = parseInput("");

    REQUIRE( ast.size() == 0);

}

TEST_CASE("On skill being found, def_skill with one skill returned", PARSER_TAG) {
    auto ast = parseInput("skill Cool (Presence)");

    REQUIRE( ast.size() == 1 );
    auto def = ast[0];

    REQUIRE(def->type == def_skill );
    
    auto skill = def->skills;
    REQUIRE( skill->name == string("Cool"));
    REQUIRE( skill->attribute == attr_presence );
}

TEST_CASE("When two skills are present, def_skill will contain both skills", PARSER_TAG) {
    auto ast = parseInput("skills Guns (Agility), Navigation (Intellect)");

    REQUIRE( ast.size() == 2);

    auto def1 = ast[0];

    REQUIRE(def1->type == def_skill );
    auto skill1 = def1->skills;
    REQUIRE( skill1->name == "Guns");

    auto def2 = ast[1];
    REQUIRE(def2->type == def_skill );
    auto skill2 = def2->skills;
    REQUIRE( skill2->name == "Navigation");
}

TEST_CASE("When a talent is present, talenList will contain a talent", PARSER_TAG) {
    auto ast = parseInput("tal Clever Retort");

    REQUIRE( ast.size() == 1 );

    auto list = ast[0];
    REQUIRE(list->type == def_talent );
    REQUIRE( list->talents != nullptr );

    auto talents = list->talents;
    REQUIRE( talents->name == "Clever Retort" );
}

TEST_CASE("When two talents are present, ast will contain both", PARSER_TAG) {
    auto ast = parseInput("talents Adversary, Purchase Information");

    REQUIRE( ast.size() == 2 );

    auto first = ast[0]->talents;
    auto second = ast[1]->talents;

    REQUIRE( first->name == "Adversary" );
    REQUIRE( second->name == "Purchase Information");

}

TEST_CASE("When two talents are present and there is a newline after the comma, def_talent will contain both", PARSER_TAG) {
    auto ast = parseInput("talents Adversary,\n Purchase Information");

    REQUIRE( ast.size() == 2 );

    auto first = ast[0]->talents;
    auto second = ast[1]->talents;
    REQUIRE( first->name == "Adversary" );
    REQUIRE( second->name == "Purchase Information");

}

TEST_CASE("Package with one item returns with the one item", PARSER_TAG) {
    auto ast = parseInput("package spy: Purchase Information");

    REQUIRE( ast.size() == 1 );

    auto def = ast[0];
    REQUIRE(def->type == def_package );
    REQUIRE( def->package != nullptr );

    auto package = def->package;
    REQUIRE( package->name == "spy" );

    auto list = package->items;
    REQUIRE( list.size() == 1);
    auto it = list[0];

    REQUIRE( it->type == item_leveledItem );
    auto li = it->item;

    REQUIRE( li->name == "Purchase Information" );
    REQUIRE( li->level == 0 );
}

TEST_CASE("Package with two items returns both items", PARSER_TAG) {
    auto ast = parseInput("package tough guy: Brawn 1, \"Ranged (Light)\"");

    auto def = ast[0];
    auto package = def->package;
    auto list = package->items;
    REQUIRE( list.size() == 2 );
    auto first = list[0];
    REQUIRE( first->type == item_attributBonus );

    auto ab = first->bonus;
    REQUIRE( ab->attribute == attr_brawn );
    REQUIRE( ab->level == 1 );

    auto second = list[1];
    REQUIRE( second->type == item_leveledItem );
    auto li = second->item;
    REQUIRE( li->name == "Ranged (Light)" );
    REQUIRE( li->level == 0 );
}

TEST_CASE("Package can contain negative and positive attribute bonuses", PARSER_TAG) {
    auto ast = parseInput("PACK Meathead : Brawn +1, Intellect -1");
    auto def = ast[0];
    auto package = def->package;
    auto list = package->items;
    REQUIRE(list.size() == 2 );
    auto first = list[0];
    auto second = list[1];

    REQUIRE( first->type == item_attributBonus );
    auto ab1 = first->bonus;
    REQUIRE( ab1->attribute == attr_brawn );
    REQUIRE( ab1->level == 1 );

    REQUIRE( second->type == item_attributBonus );
    auto ab2 = second->bonus;
    REQUIRE( ab2->attribute == attr_intellect );
    REQUIRE( ab2->level == -1 );
}

TEST_CASE("Input can contain skills, talents, and packages", PARSER_TAG) {
	try {
        auto ast = parseInput("skill Guns-Heavy (Agility)\nTalent Sneaky\npackage p1: Guns-Heavy, Sneaky");

        REQUIRE( ast.size() == 3 );
        auto def1 = ast[0];
        REQUIRE(def1->type == def_skill );

        auto def2 = ast[1];
        REQUIRE(def2->type == def_talent );

        auto def3 = ast[2];
        REQUIRE(def3->type == def_package );
	} catch (exception e) {
		cerr << e.what() << endl;
	}
}

TEST_CASE("Input can contain skills, talents, and packages, used in a charcter", PARSER_TAG) {
    auto ast = parseInput(
            "skill Guns-Heavy (Agility)\n"
            "Talent Sneaky\n"
            "package p1: Guns-Heavy, Sneaky\n"
            "rival Dr Dre: p1\n");

    REQUIRE( ast.size() == 4 );
    auto characterDef = find_if(ast.begin(), ast.end(),
                                [](const shared_ptr<definition_t>& d) { return d->type == def_character; });

    REQUIRE( characterDef != ast.end() );
    character_t* character = (*characterDef)->character;
    REQUIRE( character != nullptr );
    ostringstream name;
    name << character->name;
    REQUIRE( name.str() == "Dr Dre");

    auto items = character->items;


    REQUIRE( items.size() == 1 );
    auto item = items[0];

    REQUIRE( item->type == item_leveledItem );

    auto li = item->item;
    REQUIRE( li->name == "p1" );
    REQUIRE( li->level == 0 );
}

TEST_CASE("Character can contain multiple packages", PARSER_TAG) {
    auto ast = parseInput("skill Driving (Agility)\n"
                          "skill Brawling (Brawn)\n"
                          "talent Adversary\n"
                          "package Tough Guy: Br +3, Ag +2, Int +1\n"
                          "package Gang Heavy: Brawling, Adversary, Driving\n"
                          "rival Bruiser: Tough Guy, Gang Heavy");

    auto c = find_if(ast.begin(), ast.end(), [](const shared_ptr<definition_t>& d) { return d->type == def_character; });
    REQUIRE( c != ast.end() );
    auto character = (*c)->character;

    auto list = character->items;
    auto toughguy = find_if(list.begin(), list.end(), [](const item_t* it) {return it->type == item_leveledItem && it->item->name == "Tough Guy"; });
    auto gangheavy = find_if(list.begin(), list.end(), [](const item_t* it) {return it->type == item_leveledItem && it->item->name == "Gang Heavy"; });

    REQUIRE( toughguy != list.end() );
    REQUIRE( gangheavy != list.end() );
}

TEST_CASE("Use keyword causes file to be imported and appended to AST", PARSER_TAG) {

    char srcfile[] = "settingXXXXXX";
    char filecontent[] = "talent Larceny\n";
    int fd = mkstemp(srcfile);
    write(fd, filecontent, sizeof(filecontent));
    close(fd);

    ostringstream cmd;
    cmd << "talent Sneaky" << endl
        << "use \"" << srcfile << "\"" << endl;

    auto ast = parseInput(cmd.str().c_str());

    remove(srcfile);

    REQUIRE( ast.size() == 2 );
    auto firstdef = ast[0];
    REQUIRE(firstdef->type == def_talent);
    auto firstTalent = firstdef->talents;
    
    REQUIRE( firstTalent->name == "Sneaky" );

    auto seconddef = ast[1];
    REQUIRE(seconddef->type == def_talent);
    auto secondTalent = seconddef->talents;

    REQUIRE( secondTalent->name == "Larceny" );
}

TEST_CASE("Ranged weapon parses", PARSER_TAG) {
    auto ast = parseInput("weapon Gat (Ranged Light; Damage 4; Critical 3; Range Medium )");
    REQUIRE( ast.size() == 1 );
    auto def = ast[0];
    REQUIRE( def != nullptr );
    REQUIRE(def->type == def_weapon);

    auto w = def->weapon;
    REQUIRE( w != nullptr );
    REQUIRE( w->name == "Gat" );
    REQUIRE( w->skill == "Ranged Light" );
    REQUIRE( w->damage == 4 );
    REQUIRE( w->range == rng_medium );
}

TEST_CASE("Ranged weapon parses with named segments out of order", PARSER_TAG) {
    auto ast = parseInput("weapon Heater (Boom Stick; Critical 2; Damage 7; Range Short )");

    REQUIRE( ast.size() == 1 );
    auto def = ast[0];
    REQUIRE( def != nullptr );
    REQUIRE(def->type == def_weapon);

    auto w = def->weapon;
    REQUIRE( w != nullptr );
    REQUIRE( w->name == "Heater" );
    REQUIRE( w->skill == "Boom Stick" );
    REQUIRE( w->damage == 7 );
    REQUIRE( w->crit == 2 );
    REQUIRE( w->range == rng_short );
}

TEST_CASE("Ranged weapon parses with special", PARSER_TAG) {
    auto ast = parseInput("weapon Heater (Boom Stick; Critical 2; Damage 7; Range Short; \"Special A\", \"Special 2\" )");

    REQUIRE( ast.size() == 1 );
    auto def = ast[0];
    REQUIRE( def != nullptr );
    REQUIRE(def->type == def_weapon);

    auto w = def->weapon;
    REQUIRE( w != nullptr );
    REQUIRE( w->name == "Heater" );
    REQUIRE( w->skill == "Boom Stick" );
    REQUIRE( w->damage == 7 );
    REQUIRE( w->crit == 2 );
    REQUIRE( w->range == rng_short );

    REQUIRE( w->special.size() == 2 );
    REQUIRE( w->special[0] == "Special A" );
    REQUIRE( w->special[1] == "Special 2" );
}

TEST_CASE("Melee weapon parses", PARSER_TAG) {
    auto ast = parseInput("weapon Shiv (Melee; Critical 3; Damage +2 )");

    REQUIRE( ast.size() == 1 );
    auto def = ast[0];
    REQUIRE(def->type == def_weapon);

    auto w = def->weapon;
    REQUIRE( w != nullptr );
    REQUIRE( w->name == "Shiv" );
    REQUIRE( w->skill == "Melee" );
    REQUIRE( w->damage == 2 );
    REQUIRE( w->brawl );
    REQUIRE( w->crit == 3 );
    REQUIRE( w->range == rng_engaged );
}

TEST_CASE("Malformed weapons", PARSER_TAG) {
    REQUIRE_THROWS_MATCHES(parseInput("weapon Shiv Melee; Critical 3; Damage +1)"), parse_exception, Message("Expected LPAREN but found SEMICOLON at line 1"));
    REQUIRE_THROWS_MATCHES(parseInput("weapon Shiv (Critical 3; Damage +1"), parse_exception, Message("Expected QUOTEDSTRING/WORD but found CRITICAL at line 1"));
    REQUIRE_THROWS_MATCHES(parseInput("weapon (Melee; Critical 3; Damage +1)"), parse_exception, Message("Expected QUOTEDSTRING/WORD but found LPAREN at line 1"));
    REQUIRE_THROWS_MATCHES(parseInput("weapon Shiv (Melee; Critical 3)"), parse_exception, Message("Weapon Shiv does not contain required DAMAGE phrase at line 1"));
    REQUIRE_THROWS_MATCHES(parseInput("weapon Shiv (Melee; Damage 2)"), parse_exception, Message("Weapon Shiv does not contain required CRITICAL phrase at line 1"));
}
