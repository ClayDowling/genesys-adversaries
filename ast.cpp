#include "ast.h"

attributebonus_t::~attributebonus_t() {
}

attributebonus_t &attributebonus_t::operator=(const attributebonus_t &rhs) {
    attribute = rhs.attribute;
    level = rhs.level;
    return *this;
}

ostream& operator<<(ostream& out, const attributebonus_t& ab) {
    if (out) {
        out << ab.attribute << " " << ab.level;
    }
    return out;
}


leveledItem_t::~leveledItem_t() {
}

leveledItem_t &leveledItem_t::operator=(const leveledItem_t &rhs) {
    name = rhs.name;
    level = rhs.level;

    return *this;
}

ostream& operator<<(ostream& o, const enum itemType& it) {
    if (o) {
        switch(it) {
            case item_attributBonus:
                o << "item_attributeBonus";
                break;
            case item_leveledItem:
                o << "item_leveledItem";
                break;
            case item_weapon:
                o << "item_weapon";
                break;
            default:
                o << (int)it;
                break;
        }
    }
    return o;
}


item_t::item_t(const item_t &rhs) {
    switch (rhs.type) {
        case item_leveledItem:
            item = rhs.item;
            break;
        case item_attributBonus:
            bonus = rhs.bonus;
            break;
    }
}

item_t &item_t::operator=(const item_t &rhs) {
    switch (rhs.type) {
        case item_leveledItem:
            item = rhs.item;
            break;
        case item_attributBonus:
            bonus = rhs.bonus;
            break;
    }
    return *this;
}

item_t::~item_t() {
}

skill_t::~skill_t() {
}

talent_t::~talent_t() {
}

definition_t::~definition_t() {
    switch (type) {
        case def_skill:
            delete skills;
            break;
        case def_package:
            delete package;
            break;
        case def_talent:
            delete talents;
            break;
        case def_character:
            delete character;
            break;
    }
}

ostream &operator<<(ostream &out, enum range_t r) {
    if (!out) return out;
    switch (r) {
        case rng_engaged:
            out << "Engaged";
            break;
        case rng_short:
            out << "Short";
            break;
        case rng_medium:
            out << "Medium";
            break;
        case rng_long:
            out << "Long";
            break;
        case rng_extreme:
            out << "Extreme";
            break;
        case rng_MAX:
            out << "Invalid Range";
            break;
        default:
            out << r;
            break;
    }

    return out;
}

ostream &operator<<(ostream &out, enum definitionType dt) {
    if (!out) return out;
    switch (dt) {
        case def_skill:
            out << "def_skill";
            break;
        case def_package:
            out << "def_package";
            break;
        case def_talent:
            out << "def_talent";
            break;
        case def_character:
            out << "def_character";
            break;
        case def_weapon:
            out << "def_weapon";
            break;
        default:
            out << dt;
            break;
    }

    return out;
}
