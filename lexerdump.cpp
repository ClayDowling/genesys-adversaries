//
// Created by Clay on 6/2/2022.
//

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <fstream>
using std::ifstream;

#include "lexer.h"

int main(int argc, char* argv[]) {
    if (argc == 1) {
        cerr << "usage: " << argv[0] << " sample1.adv [sample2.adv ...]" << endl;
        return 1;
    }

    for(int i=1; i < argc; ++i) {
        cout << "File " << argv[i] << ":" << endl;
        ifstream in(argv[i]);

        lexer lex(in);
        for(token tok = lex.get(); tok.type != EndOfInput; tok = lex.get()) {
            cout << tok.type << "(" << tok.value << ")" << endl;
        }

        in.close();
    }
}
