//
// Created by clay on 3/12/22.
//

#ifndef ADVERSARIES_TOKEN_H
#define ADVERSARIES_TOKEN_H

#include <string>
using std::string;

#include "attribute.h"

// Warning: If this lists changes, a make clean is essentail!  If you see weird
// test failures, make clean.
enum token_type {
    UNKNOWN,
    WORD,
    NUMBER,
    LPAREN,
    RPAREN,
    CHARACTER,
    COMMA,
    SEMICOLON,
    COLON,
    NEWLINE,
    ATTRIBUTE,
    SKILL,
    PACKAGE,
    TALENT,
    QUOTEDSTRING,
    USE,
    WEAPON,
    ENGAGED,
    SHORT,
    MEDIUM,
    LONG,
    EXTREME,
    DAMAGE,
    CRITICAL,
    RANGE,
    ENCUMBRANCE,
    EndOfInput
};

ostream& operator<<(ostream&, enum token_type);

class token {
public:

    token();
    token(enum token_type, unsigned int);
    token(enum token_type, unsigned int, string);
    token(enum token_type, unsigned int, string, attribute_t);
    token(const token&);

    enum token_type type;
    string value;
    unsigned int line;
    attribute_t attribute;

};

#endif //ADVERSARIES_TOKEN_H
