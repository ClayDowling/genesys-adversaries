//
// Created by Clay on 4/9/2022.
//

#ifndef ADVERSARIES_ATTRIBUTE_H
#define ADVERSARIES_ATTRIBUTE_H

#include <iostream>
using std::ostream;
using std::istream;

enum attribute_t {
    attr_brawn,
    attr_agility,
    attr_intellect,
    attr_cunning,
    attr_willpower,
    attr_presence,
    attr_MAX
};

ostream& operator<<(ostream& out, const attribute_t&);
istream& operator>>(istream& in, attribute_t&);

#endif //ADVERSARIES_ATTRIBUTE_H
