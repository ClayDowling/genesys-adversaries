#ifndef TOKEN_H
#define TOKEN_H

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <memory>
using std::shared_ptr;

#include "token.h"
#include "attribute.h"
#include <iostream>
using std::ostream;

struct attributebonus_t {
    ~attributebonus_t();
    attributebonus_t() : attribute(attr_MAX), level(0) {};
    attributebonus_t(attribute_t a, int lv) : attribute(a), level(lv) {};
    attributebonus_t(const attributebonus_t& rhs) : attribute(rhs.attribute), level(rhs.level) {};
    attribute_t attribute;
    int level;

    attributebonus_t& operator=(const attributebonus_t&);
};

struct namedList_t;

enum range_t {rng_engaged, rng_short, rng_medium, rng_long, rng_extreme, rng_MAX};
ostream& operator<<(ostream&, enum range_t);

struct weapon_t {

    weapon_t() : damage(0), brawl(false), crit(0), range(rng_engaged), encumbrance(0), price(0), rarity(0) {};
    weapon_t(const weapon_t& rhs) : name(rhs.name), skill(rhs.skill), damage(rhs.damage), brawl(rhs.brawl), crit(rhs.crit), range(rhs.range), special(rhs.special) {}

    string name;
    string skill;
    unsigned int damage;
    bool brawl;
    unsigned int crit;
    enum range_t range;
    int encumbrance;
    int price;
    unsigned int rarity;

    // Cannot be vector<item_t> because item_t can include weapons
    vector<string> special;
};

struct armor_t {

    armor_t() : defense(0), soak(0), encumbrance(0), price(0), rarity(1) {}
    armor_t(string n, int d, int s) : name(n), defense(d), soak(s), encumbrance(0), price(0), rarity(1) {}

    string name;
    int defense;
    int soak;
    int encumbrance;
    int price;
    int rarity;
};

struct leveledItem_t {
    ~leveledItem_t();
    leveledItem_t() : name(""), level(0) {};
    leveledItem_t(string n, int lv) : name(n), level(lv) {};
    leveledItem_t(const leveledItem_t& rhs) : name(rhs.name), level(rhs.level) {};
    string name;
    int level;

    leveledItem_t& operator=(const leveledItem_t&);
};

enum itemType {item_attributBonus, item_leveledItem, item_weapon};
ostream& operator<<(ostream& o, const enum itemType& it);

struct item_t {
    item_t() {};
    item_t(const item_t&);
    ~item_t();

    item_t& operator=(const item_t&);

    enum itemType type;
    union {
        attributebonus_t* bonus;
        leveledItem_t* item;
        weapon_t* weapon;
    };
};

struct namedList_t {
    string name;
    vector<item_t*> items;
};

struct character_t : public namedList_t {
};

struct package_t : public namedList_t {
};

struct skill_t {
    ~skill_t();
    string name;
    attribute_t attribute;
};

struct talent_t {
    ~talent_t();
    talent_t() {};
    talent_t(string n) : name(n) {};
    string name;
};

enum definitionType {def_skill, def_package, def_talent, def_character, def_weapon, def_armor};
ostream& operator<<(ostream&, enum definitionType);

struct definition_t {
    ~definition_t();
    enum definitionType type;
    union {
        skill_t *skills;
        package_t *package;
        talent_t *talents;
        character_t *character;
        weapon_t *weapon;
        armor_t *armor;
    };
};

typedef vector<shared_ptr<definition_t>> definitionList_t;

#endif
