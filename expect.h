#ifndef EXPECT_H
#define EXPECT_H

#include "ast.h"
#include "lexer.h"
#include "token.h"
#include <string>
using std::string;

#include <initializer_list>
using std::initializer_list;

token expect_token(lexer&, token_type);
token expect_token(lexer&, initializer_list<token_type>);
string expect_name(lexer&);
int expect_number(lexer&);








#endif