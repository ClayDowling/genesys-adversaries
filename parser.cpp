
#include "expect.h"
#include "parser.h"
#include "parseexception.h"

#include <memory>
using std::shared_ptr;
using std::make_shared;

#include <sstream>
using std::ostringstream;
using std::istringstream;

#include <fstream>
using std::ifstream;

#include <cstring>
using ::strdup;

#include <exception>
using std::exception;

#include <cstdio>
using ::sprintf;

definitionList_t parser::parse(lexer &src) {

    auto t = src.get();
    while (t.type != EndOfInput) {
        switch (t.type) {
            case SKILL:
                read_skillList(src);
                break;
            case TALENT:
                read_talentList(src);
                break;
            case PACKAGE:
                add_definition(read_package(src));
                break;
            case CHARACTER:
                add_definition(read_character(src));
                break;
            case WEAPON:
                add_definition(read_weapon(src));
                break;
            case USE:
                add_file(src);
                break;
        }
        t = src.get();
    }

    return definitions;
}

void parser::add_definition(skill_t *s) {
    auto newdef = make_shared<definition_t>();
    newdef->type = def_skill;
    newdef->skills = s;
    definitions.push_back(newdef);
}

void parser::add_definition(talent_t *t) {
    auto newdef = make_shared<definition_t>();
    newdef->type = def_talent;
    newdef->talents = t;
    definitions.push_back(newdef);
}

void parser::add_definition(package_t *p) {
    auto newdef = make_shared<definition_t>();
    newdef->type = def_package;
    newdef->package = p;
    definitions.push_back(newdef);
}

void parser::add_definition(character_t *c) {
    auto newdef = make_shared<definition_t>();
    newdef->type = def_character;
    newdef->character = c;
    definitions.push_back(newdef);
}

void parser::add_definition(weapon_t *w) {
    auto newdef = make_shared<definition_t>();
    newdef->type = def_weapon;
    newdef->weapon = w;
    definitions.push_back(newdef);
}

void parser::add_file(lexer &src) {
    auto filetoken = expect_token(src, QUOTEDSTRING);

    ifstream newin(filetoken.value.c_str());
    lexer newsrc(newin);
    parse(newsrc);
    newin.close();
}

bool parser::more_in_list(lexer &src) {
    auto next = src.get();
    if (next.type == COMMA) {
        auto nl = src.get();
        if (nl.type != NEWLINE) {
            src.put_back(nl);
        }
        return true;
    } else {
        src.put_back(next);
    }
    return false;
}

void parser::read_skillList(lexer &src) {

    skill_t *skill = read_skill(src);

    if (skill) {
        add_definition(skill);

        while(more_in_list(src)) {
            skill = read_skill(src);
            if (skill) add_definition(skill);
        }
    } else {
        throw parse_exception("Failed to read skill");
    }
}

skill_t *parser::read_skill(lexer &src) {

    string n = expect_name(src);

    // Empty skill names are not allowed
    if ("" == n) return nullptr;
    string skillname = n;

    expect_token(src, LPAREN);
    auto attr = expect_token(src, ATTRIBUTE);
    expect_token(src, RPAREN);

    skill_t *s = new skill_t();
    s->attribute = attr.attribute;
    s->name = skillname;

    return s;
}


void parser::read_talentList(lexer &src) {
    talent_t *t = read_talent(src);

    if (t) {
        add_definition(t);

        if (more_in_list(src)) {
            t = read_talent(src);
            add_definition(t);
        }
    }
}

talent_t *parser::read_talent(lexer &src) {
    string n = expect_name(src);

    if (n != "") {
        talent_t *t = new talent_t(n);
        return t;
    }
    return nullptr;
}

package_t *parser::read_package(lexer &src) {
    package_t* ralf = new package_t();
    read_namedList(src, *ralf);
    return ralf;
}

character_t *parser::read_character(lexer &src) {
    character_t* alfred = new character_t();
    read_namedList(src, *alfred);
    return alfred;
}

void parser::read_namedList(lexer &src, namedList_t &nl) {
    auto name = expect_name(src);
    expect_token(src, COLON);
    nl.name = name;
    read_itemList(src, nl.items);
}

void parser::read_itemList(lexer& src, vector<item_t*>& lst) {
    auto it = read_item(src);
    lst.push_back(it);
    while (more_in_list(src)) {
        lst.push_back(read_item(src));
    }
}


item_t *parser::read_item(lexer &src) {

    auto first = expect_token(src, {QUOTEDSTRING, WORD, ATTRIBUTE});
    src.put_back(first);
    item_t *it = nullptr;
    switch (first.type) {
        case token_type::QUOTEDSTRING:
        case token_type::WORD:
            it = new item_t();
            it->type = item_leveledItem;
            it->item = read_leveledItem(src);
            break;
        case token_type::ATTRIBUTE:
            it = new item_t();
            it->type = item_attributBonus;
            it->bonus = read_addtributebonus(src);
            break;
    }

    return it;
}

attributebonus_t *parser::read_addtributebonus(lexer &src) {
    auto attribute = expect_token(src, ATTRIBUTE);
    auto bonus = expect_token(src, NUMBER);
    return new attributebonus_t(attribute.attribute, std::stoi(bonus.value));
}

leveledItem_t *parser::read_leveledItem(lexer &src) {
    string name = expect_name(src);
    if (name == "") {
        throw parse_exception("Name expected, but no name given.");
    }

    int level = 0;
    auto next = src.get();
    if (next.type == token_type::NUMBER) {
        level = std::stoi(next.value);
    } else {
        src.put_back(next);
    }
    return new leveledItem_t(name, level);
}

weapon_t *parser::read_weapon(lexer &src) {
    string name = expect_name(src);

    bool brawl = false;
    int damage = -1;
    int critical = -1;
    enum range_t range(rng_engaged);
    vector<string> specials;

    expect_token(src, LPAREN);
    string skill = expect_name(src);

    for(auto tok = src.get(); tok.type != RPAREN && tok.type != EndOfInput; tok = src.get()) {
        if (tok.type == SEMICOLON) {
            auto field = src.get();
            switch(field.type) {
                case DAMAGE:
                    tok = src.get();
                    if (tok.value[0] == '+') brawl = true;
                    src.put_back(tok);
                    damage = expect_number(src);
                    break;
                case CRITICAL:
                    critical = expect_number(src);
                    break;
                case RANGE:
                    range = read_range(src);
                    break;
                case QUOTEDSTRING:
                case WORD:
                    src.put_back(field);
                    read_stringList(src, specials);
                    break;
            }
        }
    }

    if (damage < 0) {
        ostringstream msg;
        msg << "Weapon " << name << " does not contain required DAMAGE phrase";
        throw parse_exception(msg.str().c_str(), src.get_line());
    }
    if (critical < 0) {
        ostringstream msg;
        msg << "Weapon " << name << " does not contain required CRITICAL phrase";
        throw parse_exception(msg.str().c_str(), src.get_line());
    }

    weapon_t *wpn = new weapon_t();
    wpn->name = name;
    wpn->skill = skill;
    wpn->damage = damage;
    wpn->brawl = brawl;
    wpn->crit = critical;
    wpn->range = range;
    wpn->special = specials;

    return wpn;
}

void parser::read_stringList(lexer& src, vector<string>& lst) {
    lst.push_back(expect_name(src));
    while(more_in_list(src)) {
        lst.push_back(expect_name(src));
    }
}

enum range_t parser::read_range(lexer &src) {
    auto rngvalue = src.get();
    switch (rngvalue.type) {
        case ENGAGED:
            return rng_engaged;
        case SHORT:
            return rng_short;
        case MEDIUM:
            return rng_medium;
        case LONG:
            return rng_long;
        case EXTREME:
            return rng_extreme;
        default:
            return rng_MAX;
    }
}

