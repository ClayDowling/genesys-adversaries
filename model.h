#ifndef MODEL_H
#define MODEL_H

#include <string>
using std::string;
#include <map>
using std::map;

#include "ast.h"

class attribute_bonus {
public:
    attribute_bonus(const attributebonus_t& rhs) : attribute(rhs.attribute), bonus(rhs.level) {};
    attribute_bonus(attribute_t a, int b) : attribute(a), bonus(b) {};
    attribute_bonus(const attribute_bonus& rhs) : attribute(rhs.attribute), bonus(rhs.bonus) {};

    attribute_t attribute;
    int bonus;
};
ostream& operator<<(ostream&, const attribute_bonus&);
bool operator==(const attribute_bonus&, const attribute_bonus&);

class skill {
public:
    skill(string, attribute_t);
    skill(skill_t*);
    string name;
    attribute_t attribute;
};
ostream& operator<<(ostream&, const skill&);

class skill_reference {
public:
    skill_reference() : parent(nullptr), proficiency(0) {}
    skill_reference(shared_ptr<skill> par, int prof) : parent(par), proficiency(prof) {};
    shared_ptr<skill> parent;
    int proficiency;

    skill_reference& operator+=(const leveledItem_t& rhs);
    skill_reference& operator+=(const skill_reference& rhs);

};
ostream& operator<<(ostream&, const skill_reference&);

class talent {
public:
    talent() {};
    talent(string n) : name(n) {};
    talent(talent_t*);
    string name;
};
ostream& operator<<(ostream&, const talent&);

class talent_reference {
public:
    talent_reference() : parent(nullptr), level(0) {};
    talent_reference(shared_ptr<talent> t, int l) : parent(t), level(l) {};
    shared_ptr<talent> parent;
    int level;

    talent_reference& operator+=(const talent_reference& rhs);
    talent_reference& operator+=(const leveledItem_t& rhs);
};
ostream& operator<<(ostream&, const talent_reference&);

class weapon : public weapon_t {

public:
    weapon() : weapon_t() {}
    weapon(weapon_t& w) : weapon_t(w) {}
};

ostream& operator<<(ostream&, const weapon&);

class character_parts {
public:

    string name;

    map<string, skill_reference> skills;
    map<string, talent_reference> talents;
    map<string, shared_ptr<weapon_t>> weapons;
    vector<shared_ptr<attribute_bonus> > attributes;

    character_parts& operator+=(const character_parts& rhs);
};

class package : public character_parts {
public:
    package() {};
    package(package_t*);
};
ostream& operator<<(ostream&, const package&);

class character : public character_parts {
public:
    character();
    character(const character_t*);

    int attribute[attribute_t::attr_MAX];

    int proficiency(const string skillname);
    int ability(const string skillname);

    void shortform(ostream& out);
};

ostream& operator<<(ostream& out, character& c);

class model {
public:
    model();
    model(definitionList_t&);
    map<string, shared_ptr<skill> > skills;
    map<string, shared_ptr<talent> > talents;
    map<string, shared_ptr<character> > characters;
    map<string, shared_ptr<package> > packages;
    map<string, shared_ptr<weapon_t> > weapons;

    void add_skill(string name, attribute_t attribute);
    void add_talent(string name);
    void add_weapon(weapon_t&);

    skill_reference get_skill_reference(string name);
    talent_reference get_talent_reference(string name);

private:
    void load_skilllist(skill_t*);
    void load_talenlist(talent_t*);
    void load_package(package_t*);
    void load_character(character_t*);
    void itemList_to_character_parts(vector<item_t*> items, character_parts& parts);
    void load_weapon(weapon_t*);
};

struct identifySkill {
    string target;
    identifySkill(string t) : target(t) {};
    bool operator()(shared_ptr<skill> s) { return s->name == target; }
};

struct identifyTalent {
    string target;
    identifyTalent(string t) : target(t) {};
    bool operator()(shared_ptr<talent> t) { return t->name == target; }
};

struct identifyPackage {
    string target;
    identifyPackage(string p) : target(p) {};
    bool operator()(shared_ptr<package> p) { return p->name == target; }
};

class model_exception : public std::exception {
private:
    const char* message;

public:

    model_exception(const char* m);

    virtual const char* what() const noexcept;

};

ostream& operator<<(ostream& out, model_exception& me);

#endif
