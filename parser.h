#ifndef _PARSER_H_
#define _PARSER_H_

#include "lexer.h"
#include "ast.h"
#include <memory>
using std::shared_ptr;

class parser {

private:

    definitionList_t definitions;

    void add_definition(skill_t *s);
    void add_definition(talent_t *t);
    void add_definition(package_t *p);
    void add_definition(character_t *c);
    void add_definition(weapon_t *w);
    void add_file(lexer&);
    bool more_in_list(lexer&);

    enum range_t read_range(lexer&);
    skill_t* read_skill(lexer&);
    void read_skillList(lexer&);
    void read_talentList(lexer&);
    talent_t* read_talent(lexer&);
    package_t* read_package(lexer&);
    void read_stringList(lexer& src, vector<string>& lst);
    item_t* read_item(lexer&);
    weapon_t* read_weapon(lexer&);
    attributebonus_t* read_addtributebonus(lexer&);
    leveledItem_t* read_leveledItem(lexer&);
    character_t* read_character(lexer&);
    void read_namedList(lexer &src, namedList_t &nl);
    void read_itemList(lexer& src, vector<item_t*>& lst);

public:
    definitionList_t parse(lexer&);

};



#endif
